### Features

Simple +,-,*,/ operations calculator. These operations are completed in front-end. Also calculator provides opportunity to calculate trigonometric(sin,cos, tan, ctg) operations, in a separate result box. Trigonometric operations are calculated on back-end (https://gitlab.com/abulfazahmadov/backend-calculator), it has only 4 simple routes, this server can be developed further for more computation-heavy tasks that shouldn't be completed on front-end, in this case, trigonometric operations are shown for the sake of simplicity. 

Thus, back-end is dead-simple it has only 4 plain routes. Both front-end and back-end apps has certain unit tests coverage.

# This is how it looks like

![](https://i.imgur.com/Q7kTumH.png)

white buttons are calculated and processed in front-end, the result is shown in top result box. Lower Orange buttons, are processed in back-end. When you press Sin, the current number from top result box is taken, send as HTTP request to backend, and the response is shown in the lower box.


