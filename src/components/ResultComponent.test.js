import React from 'react';
import { shallow } from 'enzyme';
import ResultComponent from './ResultComponent';

describe('ResultComponent', () => {
  it('should render correctly with no props', () => {
    const wrapper = shallow(<ResultComponent />);

    expect(wrapper).toMatchSnapshot();
  });

  it('should render correctly with 123 result props', () => {
    const wrapper = shallow(<ResultComponent result={123} />);

    expect(wrapper).toMatchSnapshot();
  });
});
