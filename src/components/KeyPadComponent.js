import React, { Component } from 'react';

class KeyPadComponent extends Component {
    onClick = (e) => this.props.onClick(e.target.name);

    render() {
      return (
        <div className="button">
          <button name="(" type="button" onClick={this.onClick}>(</button>
          <button name="CE" type="button" onClick={this.onClick}>CE</button>
          <button name=")" type="button" onClick={this.onClick}>)</button>
          <button name="C" type="button" onClick={this.onClick}>C</button>

          <button name="1" type="button" onClick={this.onClick}>1</button>
          <button name="2" type="button" onClick={this.onClick}>2</button>
          <button name="3" type="button" onClick={this.onClick}>3</button>
          <button name="+" type="button" onClick={this.onClick}>+</button>

          <button name="4" type="button" onClick={this.onClick}>4</button>
          <button name="5" type="button" onClick={this.onClick}>5</button>
          <button name="6" type="button" onClick={this.onClick}>6</button>
          <button name="-" type="button" onClick={this.onClick}>-</button>

          <button name="7" type="button" onClick={this.onClick}>7</button>
          <button name="8" type="button" onClick={this.onClick}>8</button>
          <button name="9" type="button" onClick={this.onClick}>9</button>
          <button name="*" type="button" onClick={this.onClick}>x</button>

          <button name="." type="button" onClick={this.onClick}>.</button>
          <button name="0" type="button" onClick={this.onClick}>0</button>
          <button name="=" type="button" onClick={this.onClick}>=</button>
          <button name="/" type="button" onClick={this.onClick}>÷</button>

          <button name="sin" type="button" className="special-function" onClick={this.onClick}>sin</button>
          <button name="cos" type="button" className="special-function" onClick={this.onClick}>cos</button>
          <button name="tan" type="button" className="special-function" onClick={this.onClick}>tan</button>
          <button name="ctg" type="button" className="special-function" onClick={this.onClick}>ctg</button>
        </div>
      );
    }
}

export default KeyPadComponent;
