import React from 'react';
import ReactDOM from 'react-dom';
import { shallow, mount } from 'enzyme';
import KeyPadComponent from './KeyPadComponent';

const clickFn = jest.fn();

describe('ResultComponent', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<KeyPadComponent />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('should call function when onClick', () => {
    const wrapper = shallow(<KeyPadComponent onClick={clickFn} />);
    const instance = wrapper.instance();
    instance.onClick({
      target: {
        name: '2',
      },
    });

    expect(clickFn).toHaveBeenCalled();
  });

  it('should call function when click on KeyPad button', () => {
    const mountedComponent = mount(<KeyPadComponent onClick={clickFn} />);
    mountedComponent.find('button[name="2"]').simulate('click');

    expect(clickFn).toHaveBeenCalled();
  });
});
