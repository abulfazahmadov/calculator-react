import axios from 'axios';

export default axios.create({
  baseURL: 'http://localhost:3001/', // here we can set up env prod/dev/staging variables
  responseType: 'json',
});
