import React, { Component } from 'react';
import './App.css';
import ResultComponent from './components/ResultComponent';
import KeyPadComponent from './components/KeyPadComponent';
import API from './utils/API';

class App extends Component {
  constructor() {
    super();

    this.state = {
      result: '',
      trigonometric: '',
    };
  }

    onClick = (button) => {
      if (button === '=') {
        this.calculate();
      } else if (button === 'C') {
        this.reset();
      } else if (button === 'CE') {
        this.backspace();
      } else if (button === 'sin' || button === 'cos' || button === 'tan' || button === 'ctg') {
        this.calculateTrigonometric(button);
      } else {
        const { result } = this.state;
        this.setState({
          result: result + button,
        });
      }
    };

    calculate = () => {
      let checkResult = '';
      if (this.state.result.includes('--')) {
        checkResult = this.state.result.replace('--', '+');
      } else {
        checkResult = this.state.result;
      }

      try {
        this.setState({
          // eslint-disable-next-line
          result: `${eval(checkResult) || ''}`,
        });
      } catch (e) {
        this.setState({
          result: 'error',
        });
      }
    };

    calculateTrigonometric = async (type) => {
      const { result } = this.state;
      const userData = await API.get(`/formulas/${type}/${result}`);
      this.setState({
        trigonometric: userData.data.result,
      });
    };

    reset = () => {
      this.setState({
        result: '',
        trigonometric: '',
      });
    };

    backspace = () => {
      const { result } = this.state;
      this.setState({
        result: result.slice(0, -1),
      });
    };

    render() {
      return (
        <div>
          <div className="calculator-body">
            <h1>Calculator</h1>
            <ResultComponent result={this.state.result} />
            <KeyPadComponent onClick={this.onClick} />
            <ResultComponent result={this.state.trigonometric} />
          </div>
        </div>
      );
    }
}

export default App;
