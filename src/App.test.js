import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import App from './App';

describe('App component', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('should reset the result text', () => {
    const wrapper = shallow(<App />);
    const instance = wrapper.instance();

    instance.setState({
      result: '23',
    });
    instance.reset();

    expect(wrapper.state('result')).toBe('');
  });

  it('should backspace properly', () => {
    const wrapper = shallow(<App />);
    const instance = wrapper.instance();
    instance.setState({
      result: '23',
    });
    instance.backspace();

    expect(wrapper.state('result')).toBe('2');
  });

  it('should call calculate if =', () => {
    const wrapper = shallow(<App />);
    const instance = wrapper.instance();
    const spyCalculate = jest.spyOn(instance, 'calculate');

    instance.setState({
      result: '2+3',
    });
    instance.onClick('=');

    expect(spyCalculate).toHaveBeenCalled();
    expect(wrapper.state('result')).toBe('5');
  });

  it('should call reset if C', () => {
    const wrapper = shallow(<App />);
    const instance = wrapper.instance();
    const spyReset = jest.spyOn(instance, 'reset');

    instance.setState({
      result: '2+3',
    });
    instance.onClick('C');

    expect(spyReset).toHaveBeenCalled();
    expect(wrapper.state('result')).toBe('');
  });

  it('should call calculateTrigonometric', () => {
    const wrapper = shallow(<App />);
    const instance = wrapper.instance();
    const spyCalculateTrigonometric = jest.spyOn(instance, 'calculateTrigonometric');

    instance.setState({
      result: '0',
    });
    instance.onClick('cos');

    expect(spyCalculateTrigonometric).toHaveBeenCalled();
  });

  it('should call backspace if CE', () => {
    const wrapper = shallow(<App />);
    const instance = wrapper.instance();
    const spyBackspace = jest.spyOn(instance, 'backspace');

    instance.setState({
      result: '2+3',
    });
    instance.onClick('CE');

    expect(spyBackspace).toHaveBeenCalled();
    expect(wrapper.state('result')).toBe('2+');
  });

  it('should add number', () => {
    const wrapper = shallow(<App />);
    const instance = wrapper.instance();

    instance.setState({
      result: '2+3',
    });
    instance.onClick('5');

    expect(wrapper.state('result')).toBe('2+35');
  });

  it('should evaluate', () => {
    const wrapper = shallow(<App />);
    const instance = wrapper.instance();

    instance.setState({
      result: '3+0--2*5',
    });
    instance.calculate();

    expect(wrapper.state('result')).toBe('13');
  });
});
